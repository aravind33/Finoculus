package com.aj.finoculus;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import  java.util.*;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity  {
    private EditText email;
    private Button btn;
    private String ema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = (EditText)findViewById(R.id.editText);
        btn = (Button)findViewById(R.id.button);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE| WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                ema = email.getText().toString();

              if(isValidEmail(ema) == true){
                  Intent i = new Intent(MainActivity.this,Main2Activity.class);
                  startActivity(i);
              }else if(ema.length() <= 0) {
                  Toast.makeText(MainActivity.this, "Please Enter an Email ID", Toast.LENGTH_SHORT).show();
              }else{
                  Toast.makeText(MainActivity.this, "Not a Valid Email Address", Toast.LENGTH_SHORT).show();

              }

            }
        });

    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}



