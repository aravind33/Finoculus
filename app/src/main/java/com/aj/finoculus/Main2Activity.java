package com.aj.finoculus;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    private EditText ed1,ed2,ed3,ed4,ed5,ed6;
    private Button bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ed1 = (EditText)findViewById(R.id.one);
        ed2 = (EditText)findViewById(R.id.two);
        ed3 = (EditText)findViewById(R.id.three);
        ed4 = (EditText)findViewById(R.id.four);
        ed5 = (EditText)findViewById(R.id.five);
        ed6 = (EditText)findViewById(R.id.six);

        bt1 = (Button)findViewById(R.id.o);
        bt2 = (Button)findViewById(R.id.t);
        bt3 = (Button)findViewById(R.id.th);
        bt4 = (Button)findViewById(R.id.f);
        bt5 = (Button)findViewById(R.id.fi);
        bt6 = (Button)findViewById(R.id.s);
        bt7 = (Button)findViewById(R.id.se);
        bt8 = (Button)findViewById(R.id.e);
        bt9 = (Button)findViewById(R.id.n);
        View.OnClickListener list = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button pressed = (Button)view;
               ed1.requestFocus();
                if(ed1.getText().length()<1){
                    ed1.setText(pressed.getText());
                }else if(ed2.getText().length()<1){
                    ed2.setText(pressed.getText());
                }else if(ed3.getText().length()<1){
                    ed3.setText(pressed.getText());
                }else if(ed4.getText().length()<1){
                    ed4.setText(pressed.getText());
                }else if(ed5.getText().length()<1){
                    ed5.setText(pressed.getText());
                }else {
                    ed6.setText(pressed.getText());
                    ed1.setText("*");
                    ed2.setText("*");
                    ed3.setText("*");
                    ed4.setText("*");
                    ed5.setText("*");
                    ed6.setText("*");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Main2Activity.this, Main3Activity.class);
                            startActivity(i);
                        }
                    }, 1000);
                }

            }
        };
        bt1.setOnClickListener(list);
        bt2.setOnClickListener(list);
        bt3.setOnClickListener(list);
        bt4.setOnClickListener(list);
        bt5.setOnClickListener(list);
        bt6.setOnClickListener(list);
        bt7.setOnClickListener(list);
        bt8.setOnClickListener(list);
        bt9.setOnClickListener(list);



    }

}
